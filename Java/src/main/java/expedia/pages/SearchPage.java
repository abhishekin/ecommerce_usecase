package expedia.pages;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import expedia.utilities.CommonMethods;
import expedia.utilities.Sync;

public class SearchPage {

	WebDriver driver;

	public SearchPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;

	}

	@FindBy(xpath = ".//*[@id='qf-0q-destination']")
	private WebElement destination;

	@FindBy(xpath = ".//*[@id='qf-0q-localised-check-in']")
	private WebElement checkIndate;

	@FindBy(xpath = ".//*[@id='qf-0q-localised-check-out']")
	private WebElement checkOutdate;

	@FindBy(xpath = "//button[contains(text(),'Search')]")
	private WebElement searchButton;

	@FindBy(xpath = ".//*[@id='enhanced-sort']/li[5]/a")
	private WebElement sortByPrice;


	public boolean enterDestination(String Destination) {
		Sync.waitForObject(driver, destination);
		return CommonMethods.enterText(driver,"Destination", destination, Destination);
	}

	public boolean enterCheckInDate(String CheckInDate) {
		Sync.waitForObject(driver, checkIndate);
		return CommonMethods.enterDate("Check In Date", checkIndate, CheckInDate);
	}

	public boolean enterCheckOutDate(String CheckOutDate) {
		Sync.waitForObject(driver, checkOutdate);
		return CommonMethods.enterDate("Check Out Date", checkOutdate, CheckOutDate);
	}

	public boolean clickSearchButton() {
		Sync.waitForObject(driver, searchButton);
		return CommonMethods.click("Search Button", searchButton);
	}

	public boolean clickSortByPrice() {
		Sync.waitForObject(driver, sortByPrice);
		return CommonMethods.click("Sort By Price", sortByPrice);
	}
	
	public boolean scrollToEndOfWindow() {
		return CommonMethods.jsScroll("Bottom", driver);
	}

	public boolean selectSort(String sort) {
		boolean isButtonClicked = false;
		WebElement h2low = driver
				.findElement(By.xpath(".//*[@id='sort-submenu-price']/li/a[contains(text(),'" + sort + "')]"));
		Sync.waitForObject(driver, h2low);
		try {
			h2low.click();
			isButtonClicked = true;
			System.out.println("PASS - Sorted by " + sort);
		} catch (Exception e) {
			System.out.println("FAIL - Unable to Sort by " + sort + ". Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}

	public boolean selectFeautures(String features) {
		boolean isButtonClicked = false;
		WebElement Feauture = driver.findElement(
				By.xpath(".//div[@id='filter-popular-contents']/ul/li/input/following-sibling::label[contains(text(),'"
						+ features + "')]/preceding-sibling::input"));
		Sync.waitForObject(driver, Feauture);
		try {
			Feauture.click();
			isButtonClicked = true;
			System.out.println("PASS - Sorted by " + features);
		} catch (Exception e) {
			System.out.println("FAIL - Unable to Sort by " + features + " . Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}

	public void waitForSearch(int sec) {
		Sync.waitForSeconds(sec);
	}

	public void hotelDetailsinCSV() throws IOException {
		String HotelName = null;
		String xpath = ".//*[@id='listings']/ol/li";
		FileWriter writer = new FileWriter("csvOutput/hotels.csv");
		Sync.waitForSeconds(10);
		List<WebElement> hotelList = driver.findElements(By.xpath(xpath));
		for (int i = 0; i < hotelList.size(); i++) // Can Limit Search results by declaring i < 10
		{
			try {
				HotelName = hotelList.get(i).getAttribute("data-title");
				String Unsplitprice = hotelList.get(i).getAttribute("data-info");
				String price = Unsplitprice.substring(Unsplitprice.lastIndexOf("|") + 1);
				writer.append(HotelName).append(" , Rs." + price);
				writer.append('\n');
			}
			
			catch (Exception e) {
			}
		}
		writer.flush();
		writer.close();
		System.out.println("Please Check Hotels.csv file for Results");
	}
	

}
