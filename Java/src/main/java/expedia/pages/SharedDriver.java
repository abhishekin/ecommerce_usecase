package expedia.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import expedia.pagecontainer.PageContainer;


/**
 * The Class SharedDriver.
 */
public class SharedDriver {

	/** The driver. */
	public static WebDriver driver;

	/** The page container. */
	public static PageContainer pageContainer;

	/**
	 * Creates the driver.
	 */
	public static void createDriver(String strBrowser) {

		if (strBrowser.equalsIgnoreCase("FIREFOX")) {
			driver = new FirefoxDriver();

		} else if (strBrowser.equalsIgnoreCase("CHROME")) {
			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}

		pageContainer = new PageContainer(driver);
	}
}
