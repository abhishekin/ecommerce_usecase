package expedia.pagecontainer;

import org.openqa.selenium.WebDriver;

import expedia.pages.SearchPage;




public class PageContainer {

		public WebDriver driver;
		public SearchPage searchPage;
		
		public PageContainer(WebDriver driver) {
			this.driver = driver;
			initPages();
		}

		private void initPages() {
			searchPage = new SearchPage(driver);
		
		}
		
	}

