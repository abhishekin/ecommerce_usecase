package expedia.testcase;

import java.io.IOException;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import expedia.utilities.*;

public class TC01_SearchHotel extends SharedDriver {

	static String sheetName = "TC_SearchHotel";
	static int rowNum = 2;

	static SoftAssert sa = new SoftAssert();

	public static void SoftAssertTrue(boolean a) {
		sa.assertTrue(a);
	}

	@Test(priority = 1)
	public static void enterSearchDetails() {
		SoftAssertTrue(SharedDriver.pageContainer.searchPage
				.enterDestination(TestDataUtil.testData.getCellData(sheetName, "Destination", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.searchPage
				.enterCheckInDate(TestDataUtil.testData.getCellData(sheetName, "CheckInDate", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.searchPage
				.enterCheckOutDate(TestDataUtil.testData.getCellData(sheetName, "CheckOutDate", rowNum)));
		SoftAssertTrue(SharedDriver.pageContainer.searchPage.clickSearchButton());
		sa.assertAll();
	}

	@Test(priority = 2)
	public static void selectSortDetails() throws IOException {
		SoftAssertTrue(SharedDriver.pageContainer.searchPage.clickSortByPrice());
		SoftAssertTrue(SharedDriver.pageContainer.searchPage
				.selectSort(TestDataUtil.testData.getCellData(sheetName, "SortByPrice", rowNum))); // Price High to Low
		SharedDriver.pageContainer.searchPage.waitForSearch(10); 
		SoftAssertTrue(SharedDriver.pageContainer.searchPage
				.selectFeautures(TestDataUtil.testData.getCellData(sheetName, "Features", rowNum))); // Free wifi
		sa.assertAll();
	}

	@Test(priority = 3)
	public static void hotelDetails() throws IOException {
		SoftAssertTrue(SharedDriver.pageContainer.searchPage.scrollToEndOfWindow()); // To handle Infinite scrolling
		SharedDriver.pageContainer.searchPage.hotelDetailsinCSV();

	}

}
