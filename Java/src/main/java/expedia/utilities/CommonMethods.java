package expedia.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonMethods {

	public static boolean enterText(WebDriver driver ,String strLogicalName, WebElement element, String strValue) {
		boolean isValueEntered = false;
		try {
			element.sendKeys(strValue);
			Sync.waitForElement(driver, By.xpath("html/body/div[5]/table"));
			driver.findElement(By.xpath(".//*[@id='citysqm-asi0-s0']/td/div")).click();
			if (element.getAttribute("value").contains(strValue)) {
				isValueEntered = true;
				System.out.println("PASS - Entered '" + strValue + "' in " + strLogicalName + " textbox");
			} else {
				isValueEntered = false;
				System.out.println("FAIL - Unable to enter '" + strValue + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			System.out.println("FAIL - Unable to enter " + strValue + "  Exception occurred:" + e.getMessage());
		}
		return isValueEntered;
	}

	public static boolean enterDate(String strLogicalName, WebElement element, String strValue) {
		boolean isValueEntered = false;
		try {
			element.clear();
			element.sendKeys(strValue);
			element.sendKeys(Keys.TAB);
			Sync.waitForSeconds(3);
			if (element.getAttribute("value").equals(strValue)) {
				isValueEntered = true;
				System.out.println("PASS - Entered '" + strValue + "' in " + strLogicalName + " textbox");
			} else {
				isValueEntered = false;
				System.out.println("FAIL - Unable to enter '" + strValue + "' in " + strLogicalName + " textbox");
			}
		} catch (Exception e) {
			System.out.println("FAIL - Unable to enter " + strValue + "  Exception occurred:" + e.getMessage());
		}
		return isValueEntered;
	}

	public static boolean click(String strLogicalName, WebElement element) {
		boolean isButtonClicked = false;
		try {
			element.click();
			isButtonClicked = true;
			System.out.println("PASS - Clicked " + strLogicalName + " button");
		} catch (Exception e) {
			System.out.println(
					"FAIL - Unable to click " + strLogicalName + " button . Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}
	
	public static boolean jsScroll(String strLogicalName, WebDriver driver) {
		boolean isButtonClicked = false;
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,document.body.scrollHeight)"); 
			isButtonClicked = true;
			System.out.println("PASS - Scrolled to " + strLogicalName );
		} catch (Exception e) {
			System.out.println("FAIL - Unable to Scroll to " + strLogicalName + "  Exception occurred:" + e.getMessage());
		}

		return isButtonClicked;
	}
	
	 


}